# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repo contains the setup for a basic web development environment
The ELL team will build off it on their own branches to learn react and redux

### How do I get set up? ###

git clone
git checkout dev
npm install
npm start

### Contribution guidelines ###

Built with the inspiration of Cory House's React and Redux course on Pluralsight

### Who do I talk to? ###

If you have any questions ask me, Kristine